import geopandas as gpd

def build_poishex_dict(gdf_pois,hexcover):
    gdf_pois.drop('grid_id',axis=1, errors='ignore') #handle eventual errors from this
    
    gdf_pois = gdf_pois.sjoin(hexcover[['geometry','grid_id']])[['osm_id','grid_id']]
    
    pois_hex_dict = gdf_pois.set_index('osm_id')['grid_id'].to_dict()
    return pois_hex_dict
