def myHaversine(lonlat1, lonlat2=None):
    '''
    Given one or two lists Nx*d and Ny*d (where d is the dimension)
    of (lon, lat) IN RADIANS it returns the distance matrix
    Nx*Ny IN METERS between the couples of points.
    '''
    haversine = DistanceMetric.get_metric("haversine").pairwise
    EARTH_RADIUS = 6372.795 * 1000

    _PI = np.arccos(-1)

    
    lonlat1 = np.array(lonlat1)
    if len(lonlat1.shape)==1:
        lonlat1 = lonlat1[np.newaxis,:]
        
    latlon1 = lonlat1[:,::-1]
    latlon1 = np.array(latlon1)/180*_PI
    if lonlat2 is None: 
        latlon2 = latlon1
    else:
        lonlat2 = np.array(lonlat2)
        if len(lonlat2.shape)==1:
            lonlat2 = lonlat2[np.newaxis,:]
        latlon2 = lonlat2[:,::-1]
        latlon2 = np.array(latlon2)/180*_PI
    
    return EARTH_RADIUS * haversine(latlon1, latlon2)