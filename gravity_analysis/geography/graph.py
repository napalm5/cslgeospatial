from gravity_analysis.utils.datastructures import AutoVivification
from gravity_analysis.geography.hexes import build_poishex_dict

from abc import ABC, abstractmethod
import itertools as it

import networkx as nx
import networkx.algorithms.community as communities
import igraph as ig
import geopandas as gpd
from geopy.distance import great_circle as gdistance
import numpy as np
from sklearn.metrics.pairwise import haversine_distances
from math import radians


class HexesToGraph(ABC):
    @abstractmethod
    def to_graph(self,) -> nx.Graph:
        pass
    
    def hexes_to_nodes(self, hexcover: gpd.GeoDataFrame) -> list:
        nodes = [(i, {'lat':row.centroid_lat, 'lon':row.centroid_lon}) for i,row in hexcover.iterrows()]
        return nodes
  
class RandomGraph(HexesToGraph):
    def __init__(self, n_edges=1000):
        self.n_edges = n_edges
    
    def to_graph(self, hexcover: gpd.GeoDataFrame,
                *args, **kwargs  ##HACK, interface should be refactored
                ) -> nx.Graph:
        G = nx.DiGraph()
        
        nodes = self.hexes_to_nodes(hexcover)
        edges = np.random.choice(range(len(nodes)), size=(self.n_edges,2))   
        
        G.add_nodes_from(nodes)
        G.add_edges_from(edges)
        
        return G

class RandomPOIsGraph(HexesToGraph):
    def __init__(self, 
                 cat: str = 'avg',
                 field: str = 'distance',
                 field_thresh: int = 1200):
        self.cat = cat
        self.field_thresh = field_thresh        
    
    ## TODO: custom arguments should be put in constructor,
    ## In order to respect Liskov principle
    def to_graph(self, 
                 hexcover: gpd.GeoDataFrame, 
                 gdf_pois: gpd.GeoDataFrame
                ) -> nx.DiGraph:
        
        if self.cat != 'avg':
            gdf_pois = gdf_pois[gdf_pois.category_cleaned == self.cat]
        
        poi_hex_dict = build_poishex_dict(gdf_pois, hexcover)

        # Random edges
        ## Take nb of pois in the city
        nb_pois = len(poi_hex_dict)
        ## put each POI in the center of each hexagon, with uniform probability
        poi_idx_dict_random = {}
        for poi in poi_hex_dict.keys():
            cell = np.random.choice(hexcover.index)
            poi_idx_dict_random[poi] = cell
            

                    
        graph_cpd = {idx : [] for idx in hexcover.index}
        for i,row1 in hexcover.iterrows():
            for j, row2 in hexcover.iterrows():
                hex1 = i #row1.grid_id ## This is the index, not grid_id
                hex2 = j #row2.grid_id ## To be consistend with cpd
                distance = gdistance((row1.geometry.centroid.x,row1.geometry.centroid.y),\
                                     (row2.geometry.centroid.x,row2.geometry.centroid.y)) # LON, LAT 
                if distance < self.field_thresh:
                    pois_in_hex = [k for k,v in poi_idx_dict_random.items() if v == hex2]
                    graph_cpd[hex1].append(pois_in_hex)
                    #import pdb
                    #pdb.set_trace()

                                             

        #graph_cpd = cpd_to_graphcpd(closest_pois_dict, cat = self.cat, field = self.field, field_thresh=self.field_thresh)
        
        
        G = nx.DiGraph()
        
        nodes = self.hexes_to_nodes(hexcover)
        edges = []   
        
        for idx,row_i in hexcover.iterrows():
            pois = graph_cpd[idx]
        
            hexes = [poi_idx_dict[poi] for poi in pois if poi in poi_idx_dict.keys()]
            hexes_freq = {i:hexes.count(i) for i in hexes}
            for h, f in hexes_freq.items():
                if f > 0:
                    edges.append((h, idx, f))
                    
    
        G.add_nodes_from(nodes)
        G.add_weighted_edges_from(edges, weight='n_trips')
        
        return G
        
class ReachablePOIsGraph(HexesToGraph):
    def __init__(self, 
                 cat: str = 'avg',
                 field = 'distance',
                 field_thresh: int = 1200):
        
        self.cat = cat
        self.field = field
        self.field_thresh = field_thresh
    
    def create_graphcpd(self, 
                 hexcover: gpd.GeoDataFrame, 
                 gdf_pois: gpd.GeoDataFrame
                ):
        dists = haversine_distances([[radians(cen) for cen in x.centroid.coords[0]] for x in hexcover['geometry']], 
                                    [[radians(xc) for xc in xp['geometry'].coords[0]] for i, xp in gdf_pois.iterrows()])
        radius_pois_dist = 3000
        earth_radius = 6371000
        pois_per_hex_mat = (dists * earth_radius) < self.field_thresh
        graph_cpd = dict(zip(hexcover.index, [gdf_pois.osm_id.values[pois_mask] for pois_mask in pois_per_hex_mat]))
        return graph_cpd
    
    def to_graph(self, 
                 hexcover: gpd.GeoDataFrame, 
                 gdf_pois: gpd.GeoDataFrame
                ) -> nx.DiGraph:

        if self.cat != 'avg':
            gdf_pois = gdf_pois[gdf_pois.category_cleaned == self.cat]
        
        graph_cpd = self.create_graphcpd(hexcover, gdf_pois)
        poi_hex_dict = build_poishex_dict(gdf_pois, hexcover)
        hex_idx_dict = {v:k for k,v in hexcover.grid_id.to_dict().items()}
        poi_idx_dict = {p:hex_idx_dict[h] for p,h in poi_hex_dict.items()}
        
        
        G = nx.DiGraph()
        
        nodes = self.hexes_to_nodes(hexcover)
        edges = []   

        for idx,row_i in hexcover.iterrows():
            pois = graph_cpd[idx]
        
            hexes = [poi_idx_dict[poi] for poi in pois if poi in poi_idx_dict.keys()]
            hexes_freq = {i:hexes.count(i) for i in hexes}
            for h, f in hexes_freq.items():
                if f > 0:
                    edges.append((h, idx, f))
                    
    
        G.add_nodes_from(nodes)
        G.add_weighted_edges_from(edges, weight='n_trips')

        return G
        
        
class ReachablePOIsGraphOLD(HexesToGraph):
    def __init__(self, 
                 cat: str = 'avg',
                 field = 'distance',
                 field_thresh: int = 1200):
        
        self.cat = cat
        self.field = field
        self.field_thresh = field_thresh
    
    def to_graph(self, hexcover: gpd.GeoDataFrame, 
                 closest_pois_dict: dict,
                 poi_hex_dict: dict
                ) -> nx.DiGraph:
        
        graph_cpd = cpd_to_graphcpd(closest_pois_dict, cat = self.cat, field = self.field, field_thresh=self.field_thresh)
        hex_idx_dict = {v:k for k,v in hexcover.grid_id.to_dict().items()}
        poi_idx_dict = {p:hex_idx_dict[h] for p,h in poi_hex_dict.items()}
        
        
        G = nx.DiGraph()
        
        nodes = self.hexes_to_nodes(hexcover)
        edges = []   

        for idx,row_i in hexcover.iterrows():
            pois = graph_cpd[idx]
        
            hexes = [poi_idx_dict[poi] for poi in pois if poi in poi_idx_dict.keys()]
            hexes_freq = {i:hexes.count(i) for i in hexes}
            for h, f in hexes_freq.items():
                if f > 0:
                    edges.append((h, idx, f))
                    
    
        G.add_nodes_from(nodes)
        G.add_weighted_edges_from(edges, weight='n_trips')
        
        return G
    
class CommonPOIsGraph(HexesToGraph):
    def __init__(self):
        pass
    
    def to_graph(self, hexcover: gpd.GeoDataFrame, closest_pois_dict: dict) -> nx.Graph:
        G = nx.Graph()

        nodes = self.hexes_to_nodes(hexcover)
        edges = []
        cpd_ff = cpd_f.copy()
        for i in list(cpd_ff): #is there a better way?
            cur_pois = set(cpd_ff.pop(i))
            neighbors = []
            for j,pois in cpd_ff.items():

                if len(cur_pois.intersection(set(pois))) > int(len(cur_pois)*0): # or use a threshold
                    neighbors.append(j)
                    edges.append((i,j))    


        G.add_nodes_from(nodes)
        G.add_edges_from(edges)

        return G
    
    
    
def cpd_to_graphcpd(closest_pois_dict, cat='avg', field_thresh=1200, field='distance'):
    if cat == 'avg':
        closest_pois_dict = {k : list(it.chain(*[[ v['osmid'] for v in vv if v[field]<field_thresh] for kk,vv in val.items()])) for k,val in closest_pois_dict.items()}
    else:
        closest_pois_dict = {k : [v['osmid'] for v in val[cat] if v[field]<field_thresh] for k,val in closest_pois_dict.items()}
       
    return closest_pois_dict


def associate_community(idx,partition):
    for i,comm in enumerate(partition):
        if idx in comm:
            return i 
        
    return np.nan


class PartitionGraph(ABC):
    def __init__(self, min_len_partition=5):
        self.min_len_partition = min_len_partition
        
    @abstractmethod
    def get_partition(self):
        pass
    
    
class networkxPartition(PartitionGraph):
    #def __init__(self):
    #   pass
    
    def get_partition(self, G):
        communities_generator = communities.louvain_communities(G)  #,weight='n_trips') #1
        partition = list(communities_generator)
        partition = [p for p in partition if len(p)> 20] #15
        
        return partition
    
class iGraphPartition(PartitionGraph):
    #def __init__(self,min_len_partition):
    #    pass
    
    def get_partition(self, G):
        IG = ig.Graph.from_networkx(G)
        comm = IG.community_infomap()
        partition = [c for c in comm if len(c) > self.min_len_partition]
        
        return partition
