import numpy as np
import geopandas as gpd
import contextily as ctx
import matplotlib.pyplot as plt
from shapely.ops import cascaded_union
from shapely.geometry import Polygon
from geovoronoi import voronoi_regions_from_coords, points_to_coords

crs = 'EPSG:3857'

class Voronoi():
    """
    Wrapper around geopandas and geovoronoi to obtain
    a Voronoi grid from a set of points.
    TODO: should accept both GeoSeries and list of geometries
    """
    def __init__(self, boundary_gdf: gpd.GeoDataFrame, country: str = 'italy'):
        self.crs = 'EPSG:3857' #this should depend on country
        boundary_gdf['dummy'] = 1
        self.boundary = cascaded_union(Polygon(boundary_gdf.to_crs(self.crs)\
                                                .dissolve('dummy')\
                                                .exterior\
                                                .geometry\
                                                .values[0]))
    
    
    def voronoi_grid(self, points_gdf: gpd.GeoSeries) -> gpd.GeoDataFrame:        
        orig_crs = points_gdf.crs
        coords = points_to_coords(points_gdf.to_crs(self.crs).geometry.values)
        
        poly_shapes,poly_to_pt_assignments = voronoi_regions_from_coords(coords, self.boundary)
        
        vorcover = gpd.GeoDataFrame(index=poly_shapes.keys(), geometry=list(poly_shapes.values()), crs=self.crs)
        vorcover['id'] = vorcover.index
        vorcover = vorcover.to_crs(orig_crs)
        
        return vorcover