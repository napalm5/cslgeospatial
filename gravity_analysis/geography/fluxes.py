import dask.dataframe as dd
import dask_geopandas as dg
import geopandas as gpd
import numpy as np
import pandas as pd

from abc import ABC, abstractmethod


class ComputeFluxes():
    """
    Compute fluxes between cells, starting from a set of 
    cell-to-cell trips described by a Dask GDF.
    Returns a Pandas df
    """
    
    def __init__(self,func: str ="mean"):
        self.func = func #aggregation function
        pass
    
    def get_fluxdf(self,trajs):
        # In case of undirected fluxes
        #trajs['grid_min'] = trajs[['grid_i','grid_f']].min(axis=1)
        #trajs['grid_max'] = trajs[['grid_i','grid_f']].max(axis=1)
        
        aggs = trajs.groupby(['grid_id_i', 'grid_id_f']).agg({"cluster_i": "count", "tot_time": self.func, "tot_space": self.func})
        aggs = aggs.rename(columns={"cluster_i":"trip_count","tot_time":"agg_time","tot_space":"agg_space"})
        
        return aggs.compute().reset_index()
    
class ReaggregateFluxes():
    """
    ReAggregate fluxes from grid cells to any aggregation of grid cells
    """
    def __init__(self,new_level: str, aggregations: list = ['trip_count', 'agg_time', 'agg_space']):
        self.new_level = new_level
        self.aggregations = aggregations
    
    def get_reaggr_fluxes(self, aggs: pd.DataFrame, mapper: pd.DataFrame) -> pd.DataFrame:        
        aggregations = aggs.columns.drop(['grid_id_i','grid_id_f'])
        
        aggs = aggs.merge(mapper[['grid_id', self.new_level]], left_on='grid_id_i',right_on='grid_id')\
                   .rename({'cluster' : f'{self.new_level}_i'},axis=1)\
                   .drop('grid_id',axis=1)\
                   .merge(mapper[['grid_id','cluster']], left_on='grid_id_f',right_on='grid_id')\
                   .rename({'cluster' : f'{self.new_level}_f'},axis=1)\
                   .drop('grid_id',axis=1)\
                   .groupby([f'{self.new_level}_i',f'{self.new_level}_f'])[aggregations]\
                   .sum().reset_index()
        
        return aggs
    
class FluxesHeatmap():
    """
    Transform DF of aggregated trajectories into Heatmap of fluxes
    between cells.
    Outputs Numpy array
    """
    
    def __init__(self, metric: str ='trip_count', level: str = 'grid_id'):
        self.metric = metric
        self.level = level

    def get_heatmap(self, aggs: pd.DataFrame, hexcover: gpd.GeoDataFrame) -> np.array:
        """ 
        Aggs is the trajectories DF aggregated over [grid_i, grid_f], or other
        custom aggregation levels
        """
        """
        #Originally only grid_id was allowed
        n_cells = len(hexcover)
        idx2grid = hexcover.grid_id.to_dict()
        grid2idx = dict((v, k) for k, v in idx2grid.items())
        
        heatmap = np.empty((n_cells,n_cells))
        heatmap[:] = np.nan
        
        """
        hexcover = hexcover.groupby(self.level).sum().reset_index()
        n_levels = len(hexcover)
        idx2grid = hexcover[self.level].to_dict()
        grid2idx = dict((v, k) for k, v in idx2grid.items())

        heatmap = np.empty((n_levels,n_levels))
        heatmap[:] = np.nan
        
        for i,row in aggs.iterrows():
            """grid_i = row.grid_id_i
            grid_f = row.grid_id_f
            idx_i = grid2idx[grid_i]
            idx_f = grid2idx[grid_f]
            """   
            level_i = row[f'{self.level}_i']
            level_f = row[f'{self.level}_f']
            idx_i = grid2idx[level_i]
            idx_f = grid2idx[level_f]

            heatmap[idx_i,idx_f] = row[self.metric]
    
        return heatmap
    
    def trim_heatmap(heatmap: np.array) -> np.array:
        mask = np.all(np.isnan(heatmap), axis=1)
        heatmap = heatmap[~mask]
        mask = np.all(np.isnan(heatmap), axis=0)
        heatmap = heatmap[:,~mask]
        
        return heatmap
