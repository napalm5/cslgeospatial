import dask.dataframe as dd
import dask_geopandas as dg
import geopandas as gpd

from os.path import join as pjoin
from abc import ABC, abstractmethod

class TrajLoader(ABC): 
    @abstractmethod 
    def load_trajs(self):
        pass


class DaskTrajLoader(TrajLoader):
    """ 
    Read all trajectories in path with Dask-GPD
    and return Dask DF
    """
    def __init__(self,trajs_dir,city):
        self.city = city
        self.trajs_dir = trajs_dir
        #self.hex_path = f'/home/apache/Citychrone/{city}/geometric_hexagons.geojson'
        self.hex_loader = GeoJSONLoader(directory=f'/home/apache/Citychrone/{city}',filename ='geometric_hexagons.geojson')
            
    def load_trajs(self):                
        hexcover = self.hex_loader.load() #gpd.read_file(self.hex_path)
        crs = hexcover.crs #{'init' :'epsg:4326'}
        
        df = dd.read_parquet(self.trajs_dir)
        gf = df.set_geometry(dg.points_from_xy(df, x='lon_i', y='lat_i',
                     crs = crs))
        gf.crs = crs
        
        # This part should have its own class
        # Add grid id for trip start
        traj_gdf = dg.sjoin(gf, hexcover[['geometry','grid_id']], predicate="within",how='inner')
        traj_gdf = traj_gdf.drop('index_right',axis=1)
        traj_gdf = traj_gdf.rename(columns={'grid_id':'grid_id_i'})
        
        # Ad grid id for trip end
        traj_gdf = traj_gdf.set_geometry(dg.points_from_xy(traj_gdf, x='lon_f', y='lat_f',
                                         crs = crs))
        traj_gdf = dg.sjoin(traj_gdf, hexcover[['geometry','grid_id']], predicate="within",how='inner')
        traj_gdf = traj_gdf.drop('index_right',axis=1)
        traj_gdf = traj_gdf.rename(columns={'grid_id':'grid_id_f'})

        # Persist result
        traj_gdf = traj_gdf.to_dask_dataframe().drop('geometry',axis=1)
        traj_gdf = traj_gdf.persist()
        
        return traj_gdf
        
class DaskTrajLoaderFrance(DaskTrajLoader):
    def __init__(self,city):
        path = "/home/apache/quinzeMinutes/data_in/France/users_trajectories_parquet"
        super().__init__(trajs_dir=path,city=city)
        
class GeoJSONLoader():
    def __init__(self,directory,filename):
        self.directory = directory
        self.filename = filename
        self.path = pjoin(directory,filename)

    def load(self):
        hexes = gpd.read_file(self.path)
        return hexes
    
class HexLoader(GeoJSONLoader):
    def __init__(self,directory, city, filename="hexes.geojson", core=False):
        self.filename = filename
        self.directory = directory
        if not core: 
            self.path = pjoin(directory,city,filename)
        else:
            self.path = pjoin(directory,city+"_core",filename)
        
