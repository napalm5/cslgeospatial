import geopandas as gpd
import numpy as np
import pandas as pd

from matplotlib.axes._subplots import SubplotBase
import matplotlib.pyplot as plt

from abc import ABC, abstractmethod

class Plot(ABC):
    @abstractmethod
    def setup_plot(self) -> SubplotBase:
        pass
    
    @abstractmethod
    def plot(self):
        pass


class PlotHeatmap(Plot):
    def __init__(self, figsize=(12,12)):
        self.figsize = figsize
        pass
    
    def setup_plot(self, heatmap: np.array) -> SubplotBase:
        fig, ax = plt.subplots(figsize = self.figsize)
        ax.matshow(heatmap)
        
        return ax
        
    def plot(self, heatmap: np.array):
        ax = self.setup_plot(heatmap)
        plt.show()