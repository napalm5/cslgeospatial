import geopandas as gpd
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.axes._subplots import SubplotBase
import contextily as cx

import pdb
from abc import ABC, abstractmethod

class HexcoverPlot():
    def __init__(self, figsize=(12,12), country='italy'):
        self.figsize = figsize
        self.crs = '3857' # change if outside italy

    
    def setup_plot(self, hexcover, city, column, label=False, randomize_labels=False) -> SubplotBase:
        fig,axs = plt.subplots(figsize=self.figsize)

        plotcover = hexcover.to_crs(epsg=self.crs)
        plotcover['dummy'] = 1
        plotcover.dissolve('dummy').boundary.plot(ax=axs,alpha=0)
        cx.add_basemap(axs,source=cx.providers.Stamen.Toner)

        if randomize_labels:
            plotcover = plotcover.copy()
            randomize_dict = dict(zip(plotcover[column].values, plotcover[column].sample(frac=1.).values))
            plotcover[column] = plotcover[column].replace(randomize_dict)

            pass
        
        plotcover.plot(color='grey',alpha=0.5,ax=axs)
        plotcover.plot(column=column,ax=axs,legend=True,alpha=0.9, cmap='jet')
        axs.set_title(f"{column} for {city.capitalize()}")
        
        # if condition or different class? Probably different class, this is too messy
        if label:                
            plotcover.dissolve('cluster').reset_index()\
                     .apply(lambda x: axs.annotate(text=int(x[column]), xy=x.geometry.centroid.coords[0], ha='center'), axis=1)
        
        return axs
    
    def plot_hexcover(self, hexcover, city, column, label=False):
        axs = self.setup_plot(hexcover, city, column, label)
        plt.show()