import sys
from importlib import reload
#sys.path.append('/home/apache/quinzeMinutes/analisi_claudio/gravity_analysis/') 

import cslgeospatial.geography.graph as g

import numpy as np
import pandas as pd
import geopandas as gpd

import matplotlib.pyplot as plt



class FindClusters():
    def __init__(self, city, gdfpoisloader, hexloader, rpg, parter, IN_DIR="/home/apache/quinzeMinutes/claudio_pipeline/data_in"):
        self.city = city
        self.IN_DIR = IN_DIR
        self.gdfpoisloader = gdfpoisloader
        self.hexloader = hexloader
        self.rpg = rpg 
        self.parter = parter
        
    def cluster(self):
        gdf_pois = self.gdfpoisloader.load()  
        hexcover = self.hexloader.load() 
        #cpd = pd.read_pickle(f'{self.IN_DIR}/closest_pois_dict_{self.city}.pkl.gz')
        #cpd = pd.read_pickle(f'{self.IN_DIR}/dizionario_esagoni_{self.city}_POIs_OUT_bicycle.pkl')
        #phd = hs.build_poishex_dict(gdf_pois, hexcover)

        G = self.rpg.to_graph(hexcover, gdf_pois)

        parter = g.iGraphPartition(min_len_partition=20)
        partition = self.parter.get_partition(G)

        plotcover = hexcover
        plotcover['cluster'] = plotcover.index.map(lambda id: g.associate_community(id,partition))

        return plotcover