def nested_dict(structure: list):
    dic = {key : None for key in structure[0]}
    ### NOT WORKING
    ## Need to find a wawy to iterate over nested stuff
        
    return dic

class AutoVivification(dict):
    """Implementation of perl's autovivification feature.
    https://stackoverflow.com/questions/651794/whats-the-best-way-to-initialize-a-dict-of-dicts-in-python
    """
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value