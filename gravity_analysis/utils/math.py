def jaccard_score(set1: set, set2: set):
    set1 = set(set1)
    set2 = set(set2)
    
    union = set1.union(set2)
    intersection = set1.intersection(set2)
    
    score = len(intersection) / len(union)
    return score
    