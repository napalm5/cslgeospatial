import sys
import pickle

def importOrReload(module_name, *names):
    if module_name in sys.modules:
        reload(sys.modules[module_name])
    else:
        __import__(module_name, fromlist=names)

    for name in names:
        globals()[name] = getattr(sys.modules[module_name], name)

#if 'myModule' in sys.modules:  
#    del sys.modules["myModule"]

def write_pickle(data, filename):
    with open(filename, 'wb') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

def read_pickle(filename):
    with open(filename, 'rb') as handle:
        data = pickle.load(handle)
        
    return data
