import pathlib
from setuptools import setup

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

# This call to setup() does all the work
setup(
    name="cslgeospatial",
    version="0.0.1",
    description="Collection of high-level tools to manipulate geospatial data",
    long_description=README,
    long_description_content_type="text/markdown",
    url=".",
    author="Claudio Chiappetta",
    author_email="claudio.chiappetta@uniroma1.it",
    license="Proprietary",
    classifiers=[
        "License :: Proprietary",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.9",
    ],
    packages=["cslgeospatial"],
    include_package_data=True,
    install_requires=["numpy","pandas", "pyspark", "PyYAML", "scikit_learn", "statsmodels", "SQLAlchemy", "typer", "pytest"],
    entry_points={
        "console_scripts": [
            "realpython=casevolume.process:process",
        ]
    },
)

